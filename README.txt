CONTENTS OF THIS FILE
--------------------- 
 * INTRODUCTION
 * THEME FEATURES
 * THEME REGIONS
 * REQUIREMENTS
 * INSTALLATION
 * CONFIGURATION
 * MAINTAINERS
 
INTRODUCTION
------------
 Festeva Live, a Drupal 9,10 supported theme, developed by Dotsquares is based on Stable9 and is fully responsive.

THEME FEATURES
--------------
Drupal 9 and 10 compatible
Complete Customizability
User-Friendly Interface
Fully Responsive Design
Modern and Vibrant Elements
Flexible Content Sections
Performance-Optimized
Seamless Drupal Module Integration
and more.. 

Please visit theme page for all features.
https://www.drupal.org/project/festeva

THEME REGIONS
-------------
Topbar:
This region is typically used for a small bar at the very top of your website. It might contain contact information, social media links, or other brief content.

Header:
The header region is where you often find your site logo, site name, main navigation menu, and possibly some additional branding elements.

Navigation:
The navigation region is where secondary navigation menus, search bars, and breadcrumb navigation can be placed.

Banner:
The banner region is a prominent space often used for featuring a large image, video, or a compelling call-to-action that communicates the main message of your site.

About:
The about section is where you can provide a concise introduction or summary about your website, organization, or the purpose of the site. It's a space to capture visitors' interest and give them a quick overview.

Artists:
The artists section display information about artists, possibly using Drupal views or custom blocks.This could include profiles, images, and descriptions of different artists associated with your site.

Event Calendar:
The event calendar region is meant to display an interactive calendar showcasing events. This region can be populated using Drupal views or a dedicated event calendar module, allowing users to browse and learn about upcoming events.

Content:
The content region is the main body of your page where you display articles, blog posts, or other primary content types. It's the central area where users engage with the core information your site provides.

Pricing:
The pricing section is a designated area for presenting pricing information for your products or services. You can include details about different pricing tiers or plans, helping users make informed decisions.

Contact Details:
The contact details section is where you can display relevant contact information, such as your organization's address, phone number, email, or other methods of communication.

Footer Top:
An upper section of the footer, often used for displaying important links or additional navigation and site branding also.

Footer Links:
This section include links to important pages, terms of use, privacy policy, and other relevant links.

Query:
A region that could contain a form or block encouraging users to submit queries or questions.This could be a contact form or a simple feedback form.

Location:
This region could display a map or information about your physical location.This is particularly useful if your site has a physical presence, such as a store or office.

Footer Bottom:
A lower section of the footer,containing more links, copyright information, or secondary navigation.

You can create Custom blocks,Views according to your site's requirements and place the block to the desired region.(Go to Structure > Block layout.)

Also check the festeva theme settings ,there is menubutton field where you can add the path of your section or page for the "Buy ticket" button displayed in Navigation section.

REQUIREMENTS
------------
Festeva Live theme does not require anything beyond Drupal 9 or 10 core to work.

INSTALLATION
------------
1. Place "Festeva Live" folder to the root /themes directory.
2. Login to the site and click on "Appearance" in the top Administration menu.
3. Click on "Install and set as default" next to Festeva Live theme.

Please visit below page for detailed step by step instruction.
https://www.drupal.org/docs/extending-drupal/themes/installing-themes

CONFIGURATION
-------------
Navigate to: Administration >> Appearance >> Settings >> Festeva Live theme

MAINTAINERS
-----------
Current maintainer:
 * Gunjan Rao Naik - https://www.drupal.org/u/gunjan-rao-naik
 * Chandraveer Singh - https://www.drupal.org/u/chandraveer-singh
