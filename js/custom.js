
  (function ($) {
  
    "use strict";

    // MENU
    $('.navbar-collapse a').on(
        'click',function () {
            $(".navbar-collapse").collapse('hide');
        }
    );
    
    // CUSTOM LINK
    $('.smoothscroll').click(
        function () {
            var el = $(this).attr('href');
            var elWrapped = $(el);
            var header_height = $('.navbar').height();
  
            scrollToDiv(elWrapped,header_height);
            return false;
  
            function scrollToDiv(element,navheight)
            {
                var offset = element.offset();
                var offsetTop = offset.top;
                var totalScroll = offsetTop-navheight;
  
                $('body,html').animate(
                    {
                        scrollTop: totalScroll
                    }, 300
                );
            }
        }
    );

    //Calendar Table

    $('.view-event-schedule-view table tr td').each(
        function () {
            var tdElem = $(this);
            var url =  $(this).find('.calendar_day_event_image').data('url');
            // console.log(url);
            tdElem.css(
                {
                    "background-image":" url('"+ url +"')",
                    "background-repeat":"no-repeat",
                    "background-position": "center",
                    "background-size": "cover",
                    "box-shadow": "none",
                    "position": "relative"         
                }
            );      
        }
    );
  
  })(window.jQuery);


