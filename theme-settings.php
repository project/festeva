<?php

/**
 * @file
 * Theme settings for your festeva custom theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function festeva_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id = NULL) {

  $form['festava_settings']['menubutton'] = [
    '#type' => 'details',
    '#title' => t('MenuButton Setting'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['festava_settings']['menubutton']['sectionlink'] = [
    '#type'          => 'textfield',
    '#title'         => t('SectionLink'),
    '#default_value' => theme_get_setting('sectionlink'),
    '#description' => t('Enter the URL for your custom link.'),
  ];
    
    
  $form['festava_settings']['locationdetails'] = [
    '#type' => 'details',
    '#title' => t('Location Details'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
    
  $form['festava_settings']['locationdetails']['address'] = [
    '#type'          => 'textfield',
    '#title'         => t('Address'),
    '#default_value' => theme_get_setting('address'),
    '#description'   => t("Place this text in the widget spot on your site."),
  ];

  $form['festava_settings']['locationdetails']['mapurl'] = [
    '#type'          => 'textfield',
    '#title'         => t('Map Url'),
    '#default_value' => theme_get_setting('mapurl'),
    '#description'   => t("Place this text in the widget spot on your site."),
  ];

  $form['festava_settings']['copyright'] = [
    '#type' => 'details',
    '#title' => t('Copyright'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
    
  $form['festava_settings']['copyright']['copyrightinfo'] = [
    '#type'          => 'textfield',
    '#title'         => t('Copyright Information'),
    '#default_value' => theme_get_setting('copyrightinfo'),
    '#description'   => t("Place this text in the widget spot on your site."),
  ];
  $form['festava_settings']['queries'] = [
    '#type' => 'details',
    '#title' => t('Queries'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
    
  $form['festava_settings']['queries']['phone'] = [
    '#type'          => 'tel',
    '#title'         => t('Phone Number'),
    '#default_value' => theme_get_setting('phone'),
    '#description'   => t("Place this text in the widget spot on your site."),
  ];

  $form['festava_settings']['queries']['email'] = [
    '#type'          => 'email',
    '#title'         => t('Email'),
    '#default_value' => theme_get_setting('email'),
    '#description'   => t("Place this text in the widget spot on your site."),
  ];

}
?>